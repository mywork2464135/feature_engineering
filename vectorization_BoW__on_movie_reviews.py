
# Import CountVectorizer
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.model_selection import train_test_split
from sklearn.naive_bayes import MultinomialNB
import pandas as pd

df = pd.read_csv("C:/Users/TESS/Downloads/movie_reviews_clean.csv")
print(df)


# Create a CountVectorizer object
vectorizer = CountVectorizer(lowercase=False, stop_words='english')

# Split into training and test sets
X_train, X_test, y_train, y_test = train_test_split(df['review'], df['sentiment'], test_size=0.25)

# Fit and transform vectorizer on the training data
X_train_bow = vectorizer.fit_transform(X_train)

# Transform X_test using the same vectorizer
X_test_bow = vectorizer.transform(X_test)

# Print shape of X_train_bow and X_test_bow
print(X_train_bow.shape)
print(X_test_bow.shape)

# Create a MultinomialNB object
clf = MultinomialNB()

# Fit the classifier
clf.fit(X_train_bow, y_train)

# Measure the accuracy
accuracy = clf.score(X_test_bow, y_test)
print("The accuracy of the classifier on the test set is %.3f" % accuracy)

# Predict the sentiment of a negative review
review = "The movie was terrible. The music was underwhelming and the acting mediocre."
prediction = clf.predict(vectorizer.transform([review]))[0]
print("The sentiment predicted by the classifier is %i" % (prediction))

#The accuracy of the classifier on the test set is 0.828
#The sentiment predicted by the classifier is 0
