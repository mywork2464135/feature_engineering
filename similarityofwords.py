#Checking similarity of words
import spacy

# Load model and create Doc object
nlp = spacy.load('en_core_web_lg')

# Create the doc object
doc = nlp('I like apples and oranges')

# Compute pairwise similarity scores
for token1 in doc:
  for token2 in doc:
    print(token1.text, token2.text, token1.similarity(token2))

#Output
#I I 1.0
#I like 0.3184410631656647
#I apples 0.1975560337305069
#I and -0.0979200005531311
#I oranges 0.05048730596899986
#like I 0.3184410631656647
#like like 1.0
#like apples 0.29574331641197205
#like and 0.24359610676765442
#like oranges 0.2706858515739441
#apples I 0.1975560337305069
#apples like 0.29574331641197205
#apples apples 1.0
#apples and 0.24472734332084656
#apples oranges 0.7808241248130798
#and I -0.0979200005531311
#and like 0.24359610676765442
#and apples 0.24472734332084656
#and and 1.0
#and oranges 0.3738573491573334
#oranges I 0.05048730596899986
#oranges like 0.2706858515739441
#oranges apples 0.7808241248130798
#oranges and 0.3738573491573334
#oranges oranges 1.0
